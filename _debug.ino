//-----------------------------------------------------------------------------
// Debug functionality using serial emulation on the USB port.
//-----------------------------------------------------------------------------

#include <EEPROM.h>
#define DEBUG_RX_LEN 9
#define DEBUG_VAR_LEN 5

struct DebugState {
  char rx[DEBUG_RX_LEN];
  uint8_t rxLen;
  bool rxComplete;
  int vars[DEBUG_VAR_LEN];
};

DebugState debugState;

void _doRequest(int request, int parm1, int parm2);
void _getResponse(int request, int parm1, int parm2);
int _getNum(int idx);

//-----------------------------------------------------------------------------
// Call once in setup() to enable debugging functionality.
//-----------------------------------------------------------------------------
void debugSetup()
{
    memset(&debugState.rx, 0, DEBUG_RX_LEN);  
    debugState.rxComplete = false;
    debugState.rxLen = 0;
    Serial.begin(57600);
}

//-----------------------------------------------------------------------------
// State machine to communicate with the host.
// Call this every cycle in loop().
//-----------------------------------------------------------------------------
void debugProcess()
{    
    // Read message from serial interface.
    while (Serial.available() > 0) {
      char ch = Serial.read();
      if (ch == '\n') 
        debugState.rxComplete = true;
      if (!debugState.rxComplete && ch != '\r' && debugState.rxLen < DEBUG_RX_LEN) 
        debugState.rx[debugState.rxLen++] = ch;      
    }

    if (debugState.rxComplete) {
      // Process message.
      if (debugState.rx[0] == 'V' && debugState.rx[1] == 'S') { // Version
        Serial.println(">VS BB-PLCv1");
      }      
      else if (debugState.rx[0] == 'D' && debugState.rx[1] == 'I') { // Digital input
        Serial.print(">DI ");
        for (uint8_t input = 1; input <= 7; ++input) {
          if (input > 1) Serial.print(" ");          
          Serial.print(readDi(input));          
        }
        Serial.println();
      }
      else if (debugState.rx[0] == 'A' && debugState.rx[1] == 'I') { // Analog input
        Serial.print(">AI ");
        for (uint8_t input = 1; input <= 3; ++input) {
          if (input > 1) Serial.print(" ");        
          Serial.print(readAi(input));          
        }
        Serial.println();                  
      }        
      else if (debugState.rx[0] == 'C' && debugState.rx[1] == 'V') { // Counter value
        Serial.print(">CV ");
        for (uint8_t counter = 1; counter <= 4; ++counter) {
          if (counter > 1) Serial.print(" ");
          Serial.print(readInputCounter(counter));          
        }
        Serial.println();
      }              
      else if (debugState.rx[0] == 'C' && debugState.rx[1] == 'C') { // Counter clear
        int input = 0;
        if (_getNum(1, &input)) {
          clearInputCounter(input);
          Serial.println(">CC");
        }
      }                    
      else if (debugState.rx[0] == 'D' && debugState.rx[1] == 'O') { // Digital output
        int output = 0;
        int stateNum = 0;
        if (_getNum(1, &output) && _getNum(2, &stateNum)) {
          // Set DO
          bool state = (bool)stateNum;
          writeDo(output, state);
          Serial.println(">DO");
        } 
        else {
          // Show output values
          Serial.print(">DO ");
          for (uint8_t outp = 1; outp <= 4; ++outp) {
            if (outp > 1) Serial.print(" ");        
            Serial.print(readDo(outp));          
          }
        }
        Serial.println();        
      }        
      else if (debugState.rx[0] == 'T' && debugState.rx[1] == 'S') { // Temperature sensor
        Serial.print(">TS ");
        Serial.println(tpGetTemperature());
      }        
      else if (debugState.rx[0] == 'P' && debugState.rx[1] == 'S') { // Pressure sensor
        Serial.print(">PS ");
        Serial.println(tpGetPressure());
      }              
      else if (debugState.rx[0] == 'T' && debugState.rx[1] == 'M') { // Time value
        Serial.print(">TM ");
        if (debugState.rx[3] == 'T') Serial.print(readSecondCounter());
        Serial.println();
      }  
      else if (debugState.rx[0] == 'R' && debugState.rx[1] == 'F') { // Time value
        Serial.print(">RF ");
        Serial.print(loraGetDeviceEui());
        Serial.print(" ");
        Serial.println(loraGetCurrentStep());
      }        
      else if (debugState.rx[0] == 'R' && debugState.rx[1] == 'Q') { // Request
        int request = 0;
        int parm1 = 0;
        int parm2 = 0;
        if (_getNum(1, &request) && _getNum(2, &parm1)) {
          _getNum(3, &parm2);
          Serial.println(">RQ");
          _doRequest(request, parm1, parm2);
        }
      }       
      else if (debugState.rx[0] == 'R' && debugState.rx[1] == 'S') { // Response
        int request = 0;
        int parm1 = 0;
        int parm2 = 0;
        if (_getNum(1, &request) && _getNum(2, &parm1)) {
          _getNum(3, &parm2);
           Serial.println(">RS");
         _getResponse(request, parm1, parm2);
        }
      }   
      else if (debugState.rx[0] == 'V' && debugState.rx[1] == 'R') { // Variables
        int idx = 0;
        if (_getNum(1, &idx)) {
          if (idx < DEBUG_VAR_LEN) { 
            Serial.print(">VR ");
            Serial.println(debugState.vars[idx]);
          }
        }
      }         
      else if (debugState.rx[0] == 'S' && debugState.rx[1] == 'T') { // Setting
        int setting = 0;
        int val = 0;
        if (_getNum(1, &setting) && _getNum(2, &val)) {
          // Write request
          Serial.println(">ST");          
          writeSetting(setting, val);
        } else {
          Serial.print(">ST ");
          for (uint8_t st = 1; st <= 8; ++st) { // Limit to 8 for now
            if (st > 1) Serial.print(" ");        
            Serial.print(readSetting(st));          
          }
        }
        Serial.println();    
      }       
    }
    
    // If need, start over.
    if (debugState.rxComplete || debugState.rxLen + 1 >= DEBUG_RX_LEN) {
      memset(&debugState.rx, 0, DEBUG_RX_LEN);  
      debugState.rxComplete = false;
      debugState.rxLen = 0;
    }
}

//-----------------------------------------------------------------------------
// Called when a request is requested.
//-----------------------------------------------------------------------------
void _doRequest(int request, int parm1, int parm2) {

}

//-----------------------------------------------------------------------------
// Called when a response is requested.
//-----------------------------------------------------------------------------
void _getResponse(int request, int parm1, int parm2) {
  
}

//-----------------------------------------------------------------------------
// Write a variable that may be retrieved using the debug interface.
//-----------------------------------------------------------------------------
void writeVar(uint8_t idx, int val) {
  if (idx < DEBUG_VAR_LEN) { 
      debugState.vars[idx] = val;
  }
}

//-----------------------------------------------------------------------------
// Read a variable.
//-----------------------------------------------------------------------------
int readVar(uint8_t idx) {
  if (idx < DEBUG_VAR_LEN) { 
      return debugState.vars[idx];
  }
}

//-----------------------------------------------------------------------------
// Write a setting, which is stored in non volatile memory.
// Settings will be lost if the program memory is fully used.
//-----------------------------------------------------------------------------
void writeSetting(int setting, int val) {
    if (setting < 0 || setting > 250) return; // No room to store setting
    EEPROM.update(setting, (uint16_t)val);
/*    
    int address = setting * 2;
    
    // Write EEPROM value
    byte high = val >> 8;
    byte low = val & 0xFF; 
    EEPROM.update(address, high);
    EEPROM.update(address + 1, low);
    Serial.print("EEPROM write setting ");
    Serial.print(address);
    Serial.print(" value ");
    Serial.print(high);    
    Serial.print(low);    */
}

//-----------------------------------------------------------------------------
// Read a setting, which is stored in non volatile memory.
// Settings will be lost if the program memory is fully used.
//-----------------------------------------------------------------------------
int readSetting(int setting) {
    if (setting < 0 || setting > 250) return 0;
    uint16_t res = EEPROM.read(setting);
    return (int)res;
    
    /*int address = setting * 2;
    // Write EEPROM value
    byte high = EEPROM.read(address);
    byte low = EEPROM.read(address + 1); 
    Serial.print("EEPROM read setting ");
    Serial.print(address);
    Serial.print(" value ");
    Serial.println(high << 8 + low);       
    return (high << 8 + low);*/
}

bool _getNum(int idx, int* val) 
{
  if (debugState.rxLen < 4) return 0;
  char num[6] = {0};
  char numIdx = 0;
  int idxFound = 1;
  for (uint8_t i = 3; i < debugState.rxLen; i++) {
    if (debugState.rx[i] == ' ') {      
      if (idxFound == idx && numIdx > 0) {
        // Convert the number        
        num[numIdx] = '\0';        
        *val = atoi(num);
        return true;
      } else {
        // Next number.
        ++idxFound;
        numIdx = 0;
      }
    } else if (numIdx < 5) {
      num[numIdx++] = debugState.rx[i];
    }
    // Else: discard
  }
  if (idxFound == idx && numIdx > 0) {
    // Convert the number        
    num[numIdx] = '\0';        
    *val = atoi(num);
    return true;
  }  
  // Number not found.
  return false;  
}
