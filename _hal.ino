//-----------------------------------------------------------------------------
// RN2483 Hardware Abstraction Layer for the BB board
//-----------------------------------------------------------------------------

#include <STM32ADC.h>
#include <RTClock.h>

struct HalState {
    bool toggle1s;
    uint32_t secondCounter;
    uint32_t inputCounter[4];
    uint16_t outputStates;
};

STM32ADC adc(ADC1); 
RTClock rtc(RTCSEL_LSE); 
volatile HalState halState;

//-----------------------------------------------------------------------------

uint8 analogPins[] = {PA0, PA1, PB0};
uint8 digitalPinsIn[] = {PA4, PA5, PA6, PA7, PA8, PA13, PA15};
uint8 digitalPinsInCount[] = {PA4, PA5, PA6, PA7};
uint8 digitalPinsOut[] = {PB12, PB13, PB14, PB15};
uint16_t analogValues[sizeof(analogPins) / sizeof(uint8)];
uint16_t digitalSamples[sizeof(digitalPinsIn) / sizeof(uint8)];

//-----------------------------------------------------------------------------
// Call once in setup() to configure the I/O and peripherals.
//-----------------------------------------------------------------------------
void halSetup()
{
    halState.secondCounter = 0;
    // Set up timers.
    rtc.attachSecondsInterrupt(_isrRtc);
    
    // Set up digital I/O.
    for (uint8 i = 0; i < sizeof(digitalPinsIn) / sizeof(uint8); i++) 
        pinMode(digitalPinsIn[i], INPUT);               

    for (uint8 i = 0; i < sizeof(digitalPinsOut) / sizeof(uint8); i++) {
        pinMode(digitalPinsOut[i], OUTPUT);       
        digitalWrite(digitalPinsOut[i], LOW);
    }       
    pinMode(PA14, INPUT); // Lora RESET pin
    pinMode(PB3, OUTPUT); // Lora RESET pin
    digitalWrite(PB3, LOW);
    attachInterrupt(digitalPinsInCount[0], _isrCounterInput0, RISING);
    attachInterrupt(digitalPinsInCount[1], _isrCounterInput1, RISING);
    attachInterrupt(digitalPinsInCount[2], _isrCounterInput2, RISING);
    attachInterrupt(digitalPinsInCount[3], _isrCounterInput3, RISING);
        
    // Set up analog I/O.
    adc.calibrate();
    for (uint8  i = 0; i < sizeof(analogPins) / sizeof(uint8); i++) 
        pinMode(analogPins[i], INPUT_ANALOG);       
    adc.setSampleRate(ADC_SMPR_13_5);
    adc.setScanMode();   
    adc.setPins(analogPins, sizeof(analogPins) / sizeof(uint8));
    adc.setContinuous();
    adc.setDMA(analogValues, sizeof(analogPins) / sizeof(uint8), (DMA_MINC_MODE | DMA_CIRC_MODE), NULL);
    adc.startConversion();   
}


//-----------------------------------------------------------------------------
// Reads a digital input (1..7).
//-----------------------------------------------------------------------------
bool readDi(int input)
{
    input--;    
    if (input >= sizeof(digitalPinsIn) / sizeof(uint8))
        return false;

    return !digitalRead(digitalPinsIn[input]);
}

//-----------------------------------------------------------------------------
// Reads an analog input (1..3, raw values, 12-bit).
//-----------------------------------------------------------------------------

uint16_t readAiRaw(uint8 input)
{
    input--;    
    if (input >= sizeof(analogValues) / sizeof(uint16_t)) 
        return 0;
    
    return analogValues[input];
}

//-----------------------------------------------------------------------------
// Reads the converted analog values (1..3), 12-bit accuracy, 10 samples filtered).
// Returns currents in uA and voltages in mV.
//-----------------------------------------------------------------------------
uint16_t readAi(uint8 input)
{
    input--;    
    if (input >= sizeof(analogValues) / sizeof(uint16_t)) 
        return 0;

    float val = 0;
    for (int i = 0; i < 10; ++i) {
      val += analogValues[input];
      delayMicroseconds(20);
    }
    val /= 10.0F;
    
    if (input == 0 || input == 1) {
      // Convert to mA
      return (uint16_t)((3315000.0F * val / 4095.0F) / 120.0F);
    } else { // 0-10V input
      return (uint16_t)(3300.0F * 9.181818F * val / 4096.0F);
    }
}

//-----------------------------------------------------------------------------
// Writes an output (1..4).
//-----------------------------------------------------------------------------
void writeDo(uint8 output, bool state)
{
    output--;    
    if (output >= sizeof(digitalPinsOut) / sizeof(uint8)) 
        return;
        
    if (state)
      bitSet(halState.outputStates, output);
    else
      bitClear(halState.outputStates, output);
    
    digitalWrite(digitalPinsOut[output], state);
}

//-----------------------------------------------------------------------------
// Reads the output status.
//-----------------------------------------------------------------------------
bool readDo(uint8 output)
{
    output--;
    if (output >= (sizeof(digitalPinsOut) / sizeof(uint8)) || output < 0) 
      return false;
    return bitRead(halState.outputStates, output--);
}

//-----------------------------------------------------------------------------
// Switches return value between true/false every second.
//-----------------------------------------------------------------------------
bool toggle1s()
{
    return halState.toggle1s;
}

//-----------------------------------------------------------------------------
// Returns the value of the scond counter.
// The second counter starts at 0 at startup.
//-----------------------------------------------------------------------------
uint32_t readSecondCounter()
{
    return halState.secondCounter;
}

//-----------------------------------------------------------------------------
// Returns the counter value for an input (1..4).
// - Counter is configured to count rising edges only.
//-----------------------------------------------------------------------------
uint32_t readInputCounter(int input)
{
    input--;    
    if (input >= sizeof(halState.inputCounter) / sizeof(uint32_t)) 
        return 0;
    
    return halState.inputCounter[input];
}

//-----------------------------------------------------------------------------
// Clears the counter value for an input (1..4).
//-----------------------------------------------------------------------------
uint32_t clearInputCounter(int input)
{
    input--;    
    if (input >= sizeof(halState.inputCounter) / sizeof(uint32_t)) 
        return 0;
    noInterrupts();
    halState.inputCounter[input] = 0;
    interrupts();
}

//-----------------------------------------------------------------------------
// Returns true if a timer value is elapsed.
// - timer: contains the previously stored millis() value
// - ms:    the amount of time that has to pass before the timer should elapse.
//-----------------------------------------------------------------------------
bool isTimerElapsed(uint32_t timer, uint32_t ms) {
  uint32_t currentTime = millis();
  uint32_t setpoint;
  if (0xFFFFFFFF - timer < ms) {
    // Adjust for rollover, starting at 0.
    setpoint = ms - (0xFFFFFFFF - timer);
    return (currentTime < timer && currentTime >= setpoint);
  } else {
    setpoint = ms + timer;
    return (currentTime >= setpoint);
  }  
}

//-----------------------------------------------------------------------------

void _isrRtc() {
  halState.toggle1s = (halState.toggle1s ? false : true);
  uint32_t newSecondCounter = halState.secondCounter + 1;
  halState.secondCounter = newSecondCounter; // must be atomic  
}

void _isrCounterInput0() {
  ++halState.inputCounter[0];
}

void _isrCounterInput1() {
  ++halState.inputCounter[1];
}

void _isrCounterInput2() {
  ++halState.inputCounter[2];
}

void _isrCounterInput3() {
  ++halState.inputCounter[3];
}
