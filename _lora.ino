#define DEBUG_LORA

//-----------------------------------------------------------------------------
//RN2483 LoRa functionality for the BB board
//-----------------------------------------------------------------------------

#define LORA_TX_SIZE 33
#define LORA_RX_SIZE 48
#define UART Serial2
enum LoraStep {
  LS_IDLE,
  LS_SW_RESET,
  LS_HW_RESET,
  LS_HW_RESET2,
  LS_HW_RESET3,
  LS_MAC_PAUSE,
  LS_SET_DEVEUI,
  LS_GET_DEVEUI,
  LS_SET_APPEUI,
  LS_SET_ABP_DEVADDR,
  LS_SET_ABP_NWKSKEY,
  LS_SET_ABP_APPSKEY,
  LS_SET_OTAA_APPKEY,
  LS_SET_ADR,
  LS_SET_ADR2,
  LS_SET_POWER,
  LS_SET_SF,
  LS_MAC_SAVE,
  LS_SET_RX,
  LS_MAC_RESUME,
  LS_JOIN,
  LS_WAIT_TX,
  LS_TX,
  LS_TX_RETRY,
  LS_WAIT_RX,
  LS_SHUTDOWN,  
};

struct LoraConfig {
  bool enabled; 
  bool useAbp;
  bool useAdr;
  int power;
  int sf;
  int resetPin;
  char devEui[17];
  char devAddr[9];
  char appKey[33];
  char appEui[17];
  char nwksKey[33];
  char appsKey[33];
  bool txQueued;
  char tx[LORA_TX_SIZE];
};

struct LoraState {
  LoraStep currentStep = LS_IDLE;
  LoraStep lastStep = LS_IDLE;
  uint32_t timerStep;
  uint32_t txStartTime;
  bool configSent = false;
  char rx[LORA_RX_SIZE] = {0};
  char rxBinary[17] = {0};
  char devEui[9] = {0};
  bool rxReceived; 
};

LoraConfig loraConfig;
LoraState loraState;

//-----------------------------------------------------------------------------
// Configure to set up connection by OTAA. 
// - useAdr: set to true to enable Adaptive Data Rate (recommended). 
// - sf:     spread factor value of 7..12, where 7 is the fastest/shortest range
//           and 12 is the slowest/longest range and flight time.
// - power:  value of -3..15 dB, where 14 is the recommended value.
// - devEUI: set to "" to use internal dev EUI.
//-----------------------------------------------------------------------------
void loraSetupOtaa(bool useAdr, int sf, int power, const char* devEui, const char* appEui, const char* appKey)
{
  memset(&loraConfig, 0, sizeof(LoraConfig));
  memset(&loraState, 0, sizeof(LoraState));
  loraConfig.enabled = false;
  loraConfig.resetPin = PA14;
  loraConfig.useAbp = false;
  loraConfig.useAdr = useAdr;
  loraConfig.sf = sf;
  loraConfig.power = power;
  strncpy(loraConfig.devEui, devEui, sizeof(loraConfig.devEui) / sizeof(char));
  strncpy(loraConfig.appKey, appKey, sizeof(loraConfig.appKey) / sizeof(char));
  strncpy(loraConfig.appEui, appEui, sizeof(loraConfig.appEui) / sizeof(char));
  loraState.timerStep = millis();
  loraState.configSent = false;
  loraState.lastStep = loraState.currentStep = LS_IDLE;  
}

//-----------------------------------------------------------------------------
// Configure to set up connection by ABP. 
// - useAdr:  set to true to enable Adaptive Data Rate (recommended). 
// - sf:      spread factor value of 7..12, where 7 is the fastest/shortest range
//            and 12 is the slowest/longest range and flight time.
// - power:   -3..15 dB, where 14 is the recommended value.
// - devEUI:  set to "" to use internal dev EUI.
// - devAddr: set to "" to use last 4 bytes of devEUI.
//-----------------------------------------------------------------------------
void loraSetupAbp(bool useAdr, int sf, int power, const char* devEui, const char* appEui, const char* devAddr, const char* nwksKey, const char* appsKey)
{
  memset(&loraConfig, 0, sizeof(LoraConfig));
  memset(&loraState, 0, sizeof(LoraState));  
  loraConfig.enabled = false;
  loraConfig.resetPin = PA14;
  loraConfig.useAbp = true;
  loraConfig.useAdr = useAdr;
  loraConfig.sf = sf;
  loraConfig.power = power;
  strncpy(loraConfig.devEui, devEui, sizeof(loraConfig.devEui) / sizeof(char));
  strncpy(loraConfig.devAddr, devAddr, sizeof(loraConfig.devAddr) / sizeof(char));
  strncpy(loraConfig.nwksKey, nwksKey, sizeof(loraConfig.nwksKey) / sizeof(char));
  strncpy(loraConfig.appsKey, appsKey, sizeof(loraConfig.appsKey) / sizeof(char));  
  strncpy(loraConfig.appEui, appEui, sizeof(loraConfig.appEui) / sizeof(char));
  loraState.timerStep = millis();
  loraState.configSent = false;
  loraState.lastStep = loraState.currentStep = LS_IDLE;
}

//-----------------------------------------------------------------------------
// Enables or disables LoRa functionality.
//-----------------------------------------------------------------------------
void loraEnable(bool enabled) {
  loraConfig.enabled = enabled;
  if (enabled)
    UART.begin(57600);
  else
    UART.end();
}

//-----------------------------------------------------------------------------
// State machine to communicate with the transceiver.
// Call this every cycle.
//-----------------------------------------------------------------------------
void loraProcess()
{
  if (!loraConfig.enabled && loraState.currentStep != LS_IDLE){
    loraState.currentStep = LS_SHUTDOWN;
  }

  if (loraState.currentStep != loraState.lastStep) {
    loraState.timerStep = millis(); 
    loraState.lastStep = loraState.currentStep;
  }
  
  switch (loraState.currentStep)
  {
    case LS_IDLE:      
      if (loraConfig.enabled) {
        if (isTimerElapsed(loraState.timerStep, 5000)) {
          while (UART.available()) UART.read(); // Empty receive buffer     
          _sendRequest("sys reset");     
          loraState.currentStep = LS_SW_RESET;        
        }
      }
      break;      
          
    case LS_SW_RESET:
      if (isTimerElapsed(loraState.timerStep, 3000)) { 
        if (_getResponse("RN2483", loraState.rx)) {
          _sendRequest("mac pause");     
          loraState.currentStep = LS_MAC_PAUSE;          
        } else {
          // No or incorrect response from transceiver, try hard reset.
#ifdef DEBUG_LORA          
          Serial.println("Performing hw reset");
#endif          
          loraState.currentStep = LS_HW_RESET;
        }
      }    
      break;
      
    case LS_HW_RESET:
      loraState.configSent = false;
      pinMode(PA14, OUTPUT); 
      digitalWrite(PA14, LOW);
      loraState.currentStep = LS_HW_RESET2;
      break;
    case LS_HW_RESET2:     
      if (isTimerElapsed(loraState.timerStep, 1000)) { 
        pinMode(PA14, INPUT);         
        loraState.currentStep = LS_HW_RESET3;
      }
      break;
    case LS_HW_RESET3:
      if (isTimerElapsed(loraState.timerStep, 3000)) { 
        if (_getResponse("RN2483", loraState.rx)) {
          _sendRequest("mac pause");     
          loraState.currentStep = LS_MAC_PAUSE;          
        } else {
          // No or incorrect response from transceiver, try again after some time..
          loraState.currentStep = LS_IDLE;
        }
      }    
      break;
      
    case LS_MAC_PAUSE:
      if (isTimerElapsed(loraState.timerStep, 500)) {     
        if (_getResponse("", loraState.rx)) {
          if (loraState.configSent || strlen(loraConfig.devEui) == 0) {
            _sendRequest("mac get deveui");      
            loraState.currentStep = LS_GET_DEVEUI;
          } else {
            char tx[32] = {0};
            strcpy(tx, "mac set deveui ");
            strcat(tx, loraConfig.devEui);
            _sendRequest(tx);               
            loraState.currentStep = LS_SET_DEVEUI;
          }
        } else {
          // No or incorrect response, restart.
          loraState.currentStep = LS_IDLE;
        }
      }
      break;    
      
    case LS_SET_DEVEUI:
      if (isTimerElapsed(loraState.timerStep, 500)) {     
        if (_getResponse("ok", loraState.rx)) {
          _sendRequest("mac get deveui");      
          loraState.currentStep = LS_GET_DEVEUI;
        } else {
          // No or incorrect response, restart.
          loraState.currentStep = LS_IDLE;
        }            
      }
      break;            

    
    case LS_GET_DEVEUI:
      if (isTimerElapsed(loraState.timerStep, 500)) {     
        if (_getResponse("", loraState.rx)) {
          strncpy(loraState.devEui, loraState.rx, 8);
          char tx[32] = {0};
          strcpy(tx, "mac set appeui ");
          strcat(tx, loraConfig.appEui);
          _sendRequest(tx);                
          loraState.currentStep = LS_SET_APPEUI;
        } else {
          // No or incorrect response, restart.
          loraState.currentStep = LS_IDLE;
        }
      }
      break;       
         
    case LS_SET_APPEUI:
      if (isTimerElapsed(loraState.timerStep, 500)) {     
        if (_getResponse("ok", loraState.rx)) {
          if (loraConfig.useAbp) {
            char tx[32] = {0};
            strcpy(tx, "mac set devaddr ");
            if (strlen(loraConfig.devAddr) > 0)
              strcat(tx, loraConfig.devAddr);
            else
              strcat(tx, &loraConfig.devEui[8]);
            _sendRequest(tx);        
            loraState.currentStep = LS_SET_ABP_DEVADDR;
          } else {
            char tx[32] = {0};
            strcpy(tx, "mac set appkey ");
            strcat(tx, loraConfig.appKey);
            _sendRequest(tx);      
            loraState.currentStep = LS_SET_OTAA_APPKEY;  
          }
        } else {
          // No or incorrect response, restart.
          loraState.currentStep = LS_IDLE;
        }            
      }
      break;         
      
    case LS_SET_ABP_DEVADDR:
      if (isTimerElapsed(loraState.timerStep, 500)) {     
        if (_getResponse("ok", loraState.rx)) {
          char tx[32] = {0};
          strcpy(tx, "mac set nwkskey ");
          strcat(tx, loraConfig.nwksKey);
          _sendRequest(tx);                
          loraState.currentStep = LS_SET_ABP_NWKSKEY;
        } else {
          // No or incorrect response, restart.
          loraState.currentStep = LS_IDLE;
        }            
      }
      break;         
    case LS_SET_ABP_NWKSKEY:
      if (isTimerElapsed(loraState.timerStep, 500)) {     
        if (_getResponse("ok", loraState.rx)) {
          char tx[32] = {0};
          strcpy(tx, "mac set appskey ");
          strcat(tx, loraConfig.appsKey);
          _sendRequest(tx);                
          loraState.currentStep = LS_SET_ABP_APPSKEY;
        } else {
          // No or incorrect response, restart.
          loraState.currentStep = LS_IDLE;
        }            
      }
      break;      
    case LS_SET_ABP_APPSKEY:
      if (isTimerElapsed(loraState.timerStep, 500)) {     
        if (_getResponse("ok", loraState.rx)) {
          loraState.currentStep = LS_SET_ADR;
        } else {
          // No or incorrect response, restart.
          loraState.currentStep = LS_IDLE;
        }            
      }
      break;   

    case LS_SET_OTAA_APPKEY:
      if (isTimerElapsed(loraState.timerStep, 500)) {     
        if (_getResponse("ok", loraState.rx)) {
          loraState.currentStep = LS_SET_ADR;
        } else {
          // No or incorrect response, restart.
          loraState.currentStep = LS_IDLE;
        }            
      }
      break;     

    case LS_SET_ADR:
      if (loraConfig.useAdr)
        _sendRequest("mac set adr on");      
      else
        _sendRequest("mac set adr off");      
      loraState.currentStep = LS_SET_ADR2;
      break;
    case LS_SET_ADR2:
      if (isTimerElapsed(loraState.timerStep, 500)) {     
        if (_getResponse("ok", loraState.rx)) {
          if (loraConfig.power < -3 || loraConfig.power > 15)
            loraConfig.power = 14; // Default value
          char tx[32] = {0};
          strcpy(tx, "radio set pwr ");
          char buf[8] = {0};
          sprintf(buf, "%d", loraConfig.power);
          strcat(tx, buf);
          _sendRequest(tx);       
          loraState.currentStep = LS_SET_POWER;
        } else {
          // No or incorrect response, restart.
          loraState.currentStep = LS_IDLE;
        }            
      }
      break;        
      
    case LS_SET_POWER:
      if (isTimerElapsed(loraState.timerStep, 500)) {     
        if (_getResponse("ok", loraState.rx)) {
          if (loraConfig.sf < 7 || loraConfig.sf > 12) {
            loraConfig.sf = 12;
          }
          char tx[32] = {0};
          strcpy(tx, "radio set sf sf");
          char buf[8] = {0};
          sprintf(buf, "%d", loraConfig.sf);
          strcat(tx, buf);                    
          _sendRequest(tx);      
          loraState.currentStep = LS_SET_SF;
        } else {
          // No or incorrect response, restart.
          loraState.currentStep = LS_IDLE;
        }            
      }
      break;    
 
    case LS_SET_SF:
      if (isTimerElapsed(loraState.timerStep, 500)) {     
        if (_getResponse("ok", loraState.rx)) {
           _sendRequest("mac save");      
          loraState.currentStep = LS_MAC_SAVE;
        } else {
          // No or incorrect response, restart.
          loraState.currentStep = LS_IDLE;
        }            
      }
      break;         
     
    case LS_MAC_SAVE:
      if (isTimerElapsed(loraState.timerStep, 5000)) {     
        if (_getResponse("ok", loraState.rx)) {
          loraState.configSent = true;
          _sendRequest("radio rx 0");      
          loraState.currentStep = LS_SET_RX;
        } else {
          // No or incorrect response, restart.
          loraState.currentStep = LS_IDLE;
        }            
      }
      break;      

    case LS_SET_RX:
      if (isTimerElapsed(loraState.timerStep, 500)) {     
        if (_getResponse("ok", loraState.rx)) {
          _sendRequest("mac resume");      
          loraState.currentStep = LS_MAC_RESUME;
        } else {
          // No or incorrect response, restart.
          loraState.currentStep = LS_IDLE;
        }            
      }
      break;  

    case LS_MAC_RESUME:
      if (isTimerElapsed(loraState.timerStep, 500)) {     
        if (_getResponse("ok", loraState.rx)) {
          if (loraConfig.useAbp)
            _sendRequest("mac join abp");     
          else
           _sendRequest("mac join otaa");      
          loraState.currentStep = LS_JOIN;
        } else {
          // No or incorrect response, restart.
          loraState.currentStep = LS_IDLE;
        }            
      }
      break;       
          
    case LS_JOIN:
      if (isTimerElapsed(loraState.timerStep, 20000)) {     
        if (_getResponse("ok", loraState.rx)) {
          loraState.configSent = true;
          loraState.txStartTime = readSecondCounter();
          loraState.currentStep = LS_WAIT_TX;
        } else {
          // No or incorrect response, restart.
          loraState.currentStep = LS_IDLE;
        }            
      }
      break;               

    case LS_WAIT_TX:            
      // Re-join every x hours   
      if (readSecondCounter() - loraState.txStartTime > 36000) { // 36000: 10 hours
        while (UART.available()) UART.read(); // Flush receive buffer
        _sendRequest("mac pause");    
        loraState.currentStep = LS_MAC_PAUSE;
      } else {
        if (loraConfig.txQueued) {
           while (UART.available()) UART.read(); // Flush receive buffer
           char tx[64] = {0};
           strcpy(tx, "mac tx cnf 1 ");
           strcat(tx, loraConfig.tx);
           _sendRequest(tx);   
           loraState.currentStep = LS_TX;
        }
      }      
      break;

    case LS_TX:
      if (isTimerElapsed(loraState.timerStep, 500)) {     
        if (_getResponse("ok", loraState.rx)) {
          loraConfig.txQueued = false;         
          loraState.currentStep = LS_WAIT_RX;
        } else if (_beginsWith(loraState.rx, "no_free") || _beginsWith(loraState.rx, "busy"))  {
          // Too early to send another message, wait some time and then try again.
          loraState.currentStep = LS_TX_RETRY;
        } else {
          // No or incorrect response, restart.
          loraState.currentStep = LS_IDLE;
        }            
      }
    break;      

    case LS_TX_RETRY:
      if (isTimerElapsed(loraState.timerStep, 30000)) { 
        // Try sending the message again.
        loraState.currentStep = LS_WAIT_TX;
      }
      break;

    case LS_WAIT_RX:
      if (isTimerElapsed(loraState.timerStep, 20000)) {     
      if (_getResponse("mac_rx", loraState.rx)) {
        // Data received.
#ifdef DEBUG_LORA
        Serial.print("Received HEX data: ");
        Serial.println(loraState.rx);
#endif        
        if (strlen(loraState.rx) > 10) {
          strcpy(loraState.rxBinary, &loraState.rx[9]);
          _hexToString(loraState.rxBinary, strlen(loraState.rx) - 10);
          loraState.rxReceived = true;        
#ifdef DEBUG_LORA
          Serial.print("Converted: ");
          Serial.println(loraState.rxBinary);
#endif
        }
      } else {
 #ifdef DEBUG_LORA
        Serial.println("No receive data");
#endif       
      }
      
      loraState.currentStep = LS_WAIT_TX;          
    }
    break;          

    case LS_SHUTDOWN:
      _sendRequest("mac pause");     
      loraState.currentStep = LS_IDLE;
    break;
  }
}


//-----------------------------------------------------------------------------
// Returns true if the transceiver is ready to send requests.
//-----------------------------------------------------------------------------
bool loraReady() {
  return (loraState.currentStep == LS_WAIT_TX);
}

//-----------------------------------------------------------------------------
// Sends a message to the server uplink.
// Set message len to 0 to autodetermine based on 0 chars.
//-----------------------------------------------------------------------------
void loraSend(uint8_t* msg, int len) {
  int txIdx = 0;  
  for (uint8_t i = 0; i < (LORA_TX_SIZE - 1) / 2 && i < len; i++)
  {
    sprintf(&loraConfig.tx[txIdx], "%02x", int(msg[i]));
    txIdx += 2;
  }  
  loraConfig.tx[txIdx] = '\0';
  loraConfig.txQueued = true;  
}

//-----------------------------------------------------------------------------
// Returns true if a message to send is still queued.
// By law, a 0.1% maxium transmission duty cycle is allowed for LoRa, meaning
// that messages may be delayed by the transceiver when this duty 
// cycle is reached.
//-----------------------------------------------------------------------------
bool loraSendQueued() {
  return loraConfig.txQueued;
}

//-----------------------------------------------------------------------------
// Returns a message from the server downlink, if available.
//-----------------------------------------------------------------------------
int loraReceive(char** msg) {
  int bytes = 0;
  if (loraState.rxReceived) {
    loraState.rxReceived = false;
    *msg = loraState.rxBinary;
    return strlen(loraState.rxBinary);
  }
  return 0;
}

//-----------------------------------------------------------------------------
// Returns the LoRa device ID.
//-----------------------------------------------------------------------------
char* loraGetDeviceEui() {
  return loraState.devEui;
}

//-----------------------------------------------------------------------------
// Returns the LoRa device ID.
//-----------------------------------------------------------------------------
int loraGetCurrentStep() {
  return (int)loraState.currentStep;
}


//-----------------------------------------------------------------------------

void _sendRequest(const char* data) {
  UART.println(data);
#ifdef DEBUG_LORA
    Serial.print("TX: ");
    Serial.println(data);
#endif 
}

bool _getResponse(const char* match, char* response) {  
  int idx = 0;
  while (UART.available() && idx < LORA_RX_SIZE - 1)
    response[idx++] = UART.read();
  response[idx] = '\0';
#ifdef DEBUG_LORA
    Serial.print("RX: ");
    Serial.println(response);
#endif
  return _beginsWith(response, match);
}

bool _beginsWith(const char* msg, const char* val) {
  int msgLen = strlen(msg);
  int valLen = strlen(val);
  for (uint8_t i = 0; val && i < msgLen && i < valLen; ++i)
    if (msg[i] != val[i]) return false;
  return true;
}

void  _hexToString(char *s, int len)
{    
    int idx = 0;
    for (uint8_t i = 0; i < len - 1; i = i + 2)
    {
        int val = 0;
        if ((s[i] >= '0') && (s[i] <= '9'))
            val = s[i] - '0';
        else
            val = toupper(s[i]) - 'A' + 10;
        val = val << 4;
        if ((s[i+1] >= '0') && (s[i+1] <= '9'))
            val += s[i+1] - '0';
        else
            val += toupper(s[i+1]) - 'A' + 10;  
        s[idx++] = val;
    }
    s[idx] = '\0';
}
