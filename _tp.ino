//-----------------------------------------------------------------------------
// RN2483 pressure and temperature functionality for the BB board.
//-----------------------------------------------------------------------------

#include <Adafruit_BMP280.h>

#define BMP_SCK  (PB6)
TwoWire bmp280Wire(1, I2C_FAST_MODE);

Adafruit_BMP280 bmp(&bmp280Wire);

struct BbtpConfig {
  bool enabled; 
};

BbtpConfig bbtpConfig;

//-----------------------------------------------------------------------------
// Call once in setup() to configure the temperature and pressure.
//-----------------------------------------------------------------------------
bool tpSetup() {
  if (!bmp.begin()) 
    return false;
  bbtpConfig.enabled = true;    
  return true;
}

//-----------------------------------------------------------------------------
// Returns the temperature in 0.1 oC.
// If the interface is not available, 0 is returned.
//-----------------------------------------------------------------------------
int tpGetTemperature() {
  if (!bbtpConfig.enabled) return 0;  
  return (int)(bmp.readTemperature() * 10.0F);    
}

//-----------------------------------------------------------------------------
// Returns the pressure in Pa.
// 100 Pa equals 1 mbar.
// If the interface is not available, 0 is returned.
//-----------------------------------------------------------------------------
int32_t tpGetPressure() {
  if (!bbtpConfig.enabled) return 0;
  return (int32_t)bmp.readPressure();    
}
