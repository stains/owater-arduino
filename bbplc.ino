
#define rsSerial Serial3

bool started = false;
int alarmStatus;

int pulsesToCount = 0;

uint32_t endTime = 0;
uint32_t startTime = 0;
uint32_t lastLoraSendTime = 0;
uint32_t lastVolumeCheckTime = 0;
uint32_t lastUpdateTime = 0;
uint32_t lastSavedTime = 0;

uint32_t counterL = 0;
uint32_t counterH = 0;
uint16_t remainingVolume = 0;
bool lastCoinL = true;
bool lastCoinH = true;
bool remotePowerdown = false;
bool firstCycle = true;

int cfgPulsesL, cfgPulsesH;
int cfgTimeL, cfgTimeH;
int cfgStartPress, cfgEndPress;
int cfgLength, cfgWidth;


void setup() {
  // put your setup code here, to run once:
  halSetup();
  debugSetup();
  tpSetup();  
  
  // Settings voor Rick
  loraSetupAbp(true, 12, 15, "005EAF9F31931757", "70B3D57ED001F91B", "26011B66", "8C8E24AE4B7A6FE32AB22451BA92BF06", "63631DB46064DBBACEA9660AE994E0CA");

  // Testding
  //loraSetupAbp(true, 12, 15, "0059AC000018075F", "70B3D57ED001E9EE", "260118DD", "EB927CAD2B65FF188051D4857884A267", "0A026E4AC51F301857BFDA20EFDEECAB");

  
  loraEnable(true); 
  lastLoraSendTime = readSecondCounter();
  
  if (readSetting(202) == 123) { // L saved?
    counterL = readSetting(200) + (readSetting(201) << 16);
  }  
  if (readSetting(205) == 123) { // H saved?
    counterH = readSetting(203) + (readSetting(204) << 16);
  }  
  
}


void saveCounters() {
  if (readSecondCounter() - lastSavedTime > 14400) { // Save only every 4 hours 
    Serial.print("Saving counters... ");
    Serial.print(counterL);
    Serial.print(" ");
    Serial.println(counterH);
    writeSetting(200, counterL & 0xFFFF);
    writeSetting(201, (counterL >> 16) & 0xFFFF);
    writeSetting(202, 123); // Counter L saved
    writeSetting(203, counterH & 0xFFFF);
    writeSetting(204, (counterH >> 16) & 0xFFFF);
    writeSetting(205, 123); // Counter H saved
    lastSavedTime = readSecondCounter();
  }
}

void loop() {
  debugProcess();
  loraProcess();

  bool coinL = readDi(2);
  bool coinH = readDi(3);
  bool tankLow = !readDi(4);
  bool alarmRo = readDi(7);
  bool startRo = !readDi(6);
  uint16_t pressureCurrent = readAi(1); // mA

  // Determine if we have an alarm
  alarmStatus = 0;
  //if (emgStop) alarmStatus = 1;
  if (alarmRo) alarmStatus = 2;
  if (tankLow) alarmStatus = 3;
  if (remotePowerdown) alarmStatus = 4;
  
  bool alarm = tankLow || /*emgStop ||*/ alarmRo || remotePowerdown; 
  bool stopNow = /*emgStop ||*/ alarmRo; // Tank low should not stop current session
  writeDo(4, (bool)alarm); // Alarm contact 
    
  // Update settings 
  if ((readSecondCounter() - lastUpdateTime) > 10 || firstCycle) {
    switch (alarmStatus) {
      default: break;
      case 1: Serial.println("EMERGENCY STOP"); break;
      case 2: Serial.println("ALARM RO"); break;
      case 3: Serial.println("TANK LOW"); break;      
      case 4: Serial.println("REMOTE SHUTDOWN"); break;      
    }
    cfgPulsesL = readSetting(1);
    cfgPulsesH = readSetting(2);
    cfgTimeL = readSetting(3); // s
    cfgTimeH = readSetting(4); // s
    cfgStartPress = readSetting(5); // mbar
    cfgEndPress = readSetting(6); // mbar
    cfgLength = readSetting(7); // cm
    cfgWidth = readSetting(8); // cm

    char* data;
    int bytes = loraReceive(&data);
    if (bytes > 0)
    {
      if (data[0] == 'P' && data[1] == '1') {
        Serial.println("Powering up");
        remotePowerdown = false;
      } else if (data[0] == 'P' && data[1] == '0') {
        Serial.println("Powering down");
        remotePowerdown = true;
      }
    }
    lastUpdateTime = readSecondCounter();    
  }
  
  // Start L
  if (!alarm && !started && !lastCoinL && coinL) {
    pulsesToCount = cfgPulsesL;
    endTime = readSecondCounter() + cfgTimeL;
    counterL++;
    saveCounters();
    Serial.print("Started L #: ");
    Serial.println(counterL);
   
    startTime = readSecondCounter();
    clearInputCounter(1);
    started = true;
  }
  // Start H
  if (!alarm && !started && !lastCoinH && coinH) {
    pulsesToCount = cfgPulsesH;
    endTime = readSecondCounter() + cfgTimeH;
    counterH++;
    saveCounters();
    Serial.print("Started H #: ");
    Serial.println(counterH);
    startTime = readSecondCounter();
    clearInputCounter(1);
    started = true;
  }
    
  lastCoinL = coinL; // For detection of positive edge
  lastCoinH = coinH;

  // Stop at alarm, after timeout or after pulses reached
  if (stopNow || readSecondCounter() > endTime || readInputCounter(1) > pulsesToCount) {
    started = false;
  }

  // Control outputs
  if (started) {
      writeDo(1, true); // Pump on
      if (readSecondCounter() - startTime >= 1) {
        writeDo(2, true); // Valve on  
      }
  } else {
      writeDo(1, false); // Pump off
      writeDo(2, false); // Valve off    
  }

  // Enable Ro when startRo signal is HIGH
  writeDo(3, startRo);
  
  
  // Calculate remaining volume
  if ((readSecondCounter() - lastVolumeCheckTime) > 30 && pressureCurrent > 4000 && pressureCurrent < 24000 || firstCycle) { // Sensor OK
    float startPress = cfgStartPress * 100.0F; // mbar => Pa
    float endPress  = cfgEndPress * 100.0F; // mbar => Pa
    // Pressure is depth * density * gravity
    // Pressure is start pressure + measured current * range / current range 
    float pressureSensor = startPress + ((float)pressureCurrent - 4000.0F) * (endPress - startPress) / 16000.0F; // Pa
    Serial.print("Pressure sensor value: " );
    Serial.print(pressureSensor);
    Serial.println(" mbar");
    float pressureDelta = pressureSensor - tpGetPressure();
    Serial.print("Relative pressure diff: ");
    Serial.print(pressureDelta);
    Serial.println(" mbar");
    // Depth = pressure / (density * gravity)
    // Assume density of water is 998 kg/m3 (20 oC)
    // Assume gravity = 9.81F
    float depth = pressureDelta / (1000 * 9.81F); // m
    Serial.print("Depth: ");
    Serial.print(depth);    
    Serial.println(" m");
    if (depth <= 0) {
      remainingVolume = 0;
    } else {
      /*uint16_t diameter = 3000; // m
      float volume = 3.14159265358979F * (float)diameter/2.0F * (float)diameter/2.0F * depth;*/
      float volume = depth * ((float)cfgLength / 1000.0F) * ((float)cfgWidth / 1000.0F); // mm => m
      remainingVolume = (uint16_t)(volume * 1000); // m3 to l  
    }
    
    Serial.print("Volume: ");
    Serial.print(remainingVolume);        
    Serial.println(" l");        
    lastVolumeCheckTime = readSecondCounter();
    //  density of fresh water = 1000 kg/m^3, salt water you'll need to look up.
    // so pressure/depth = 1000 x 9.8 = 9800 (in pascal (newtons per square metre) for fresh water
  }
    
  // Send Lora status if needed
  if ((readSecondCounter() - lastLoraSendTime) > 2700) {
    if (loraReady()) {
      uint8_t msg[13] = {0};
      msg[0] = started;
      msg[1] = alarmStatus;
      msg[2] = counterL >> 24;
      msg[3] = (counterL >> 16) & 0xFF;
      msg[4] = (counterL >> 8) & 0xFF;
      msg[5] = counterL & 0xFF;
      msg[6] = counterH >> 24;
      msg[7] = (counterH >> 16) & 0xFF;
      msg[8] = (counterH >> 8) & 0xFF;
      msg[9] = counterH & 0xFF;      
      msg[10] = remainingVolume >> 8;
      msg[11] = remainingVolume & 0xFF;
      uint32_t checkSum = 0;
      for (int i = 0; i < 12; ++i) {
        checkSum += msg[i];
      }
      msg[12] = checkSum % 255;
      loraSend(msg, 13);
      lastLoraSendTime = readSecondCounter();
    }
  }

  firstCycle = false;
}

